package fit5042.tutex.calculator;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	private ArrayList<Property> propertyList;
	
	
	public ComparePropertySessionBean()
	{
		propertyList = new ArrayList<Property>();
	}
	
	@Override
	public void addProperty(Property property) {
		propertyList.add(property);
	}

	@Override
	public void removeProperty(Property property) {
		for (int index = 0; index < propertyList.size(); index++) {
			if (propertyList.get(index).getPropertyId() == property.getPropertyId()) {
				propertyList.remove(index);
			}
		}
	}

	@Override
	public int getBestPerRoom() {
		int idCheapest = 0;
		double unitPriceCheapest;
		double unitPriceCompared;
		
		// only add one property
		if (propertyList.size() == 1) {
			idCheapest = propertyList.get(0).getPropertyId();
		}
		else {
			int index = 0;
			unitPriceCheapest = propertyList.get(index).getPrice() / propertyList.get(index).getNumberOfBedrooms();
			for (; index < propertyList.size() - 1; index++) {
				
				unitPriceCompared = propertyList.get(index + 1).getPrice() / propertyList.get(index + 1).getNumberOfBedrooms();
				if (unitPriceCheapest > unitPriceCompared) {
					idCheapest = propertyList.get(index + 1).getPropertyId();
					unitPriceCheapest = unitPriceCompared;
				}
				else {
					idCheapest = propertyList.get(index).getPropertyId();
				}
			}
		}
		return idCheapest;
	}
   
    
}
