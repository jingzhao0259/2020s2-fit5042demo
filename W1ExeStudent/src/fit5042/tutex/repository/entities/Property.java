/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;


/**
 * 
 * 
 * @author Jing Zhao 29593670
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {

	private int id;
	private String address;
	private int numberOfBedrooms;
	private int size;
	private double price;

	public Property() {

	}

	/**
	 * @param id
	 * @param address
	 * @param numberOfBedrooms
	 * @param size
	 * @param price
	 */
	public Property(int id, String address, int numberOfBedrooms, int size, double price) {
		super();
		this.id = id;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.price = price;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the numberOfBedrooms
	 */
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	/**
	 * @param numberOfBedrooms the numberOfBedrooms to set
	 */
	public void setNumberOfBedrooms(int numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return  id + " " + address + " " + numberOfBedrooms + "BR(s) " + 
					new BigDecimal(size).setScale(1) + "sqm " +
					NumberFormat.getCurrencyInstance(Locale.US).format(price);
	}

}
