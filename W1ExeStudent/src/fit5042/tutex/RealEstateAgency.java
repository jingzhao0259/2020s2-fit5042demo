package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import fit5042.tutex.repository.SimplePropertyRepositoryImpl;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Jing Zhao 29593670
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;
    private SimplePropertyRepositoryImpl propertiesRepositroy;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
        propertiesRepositroy = new SimplePropertyRepositoryImpl();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	Property propertyOne = new Property(1, "24 Boston Ave, Australia", 2, 150, 420000.00);
    	Property propertyTwo = new Property(2, "11 Boston Ave, Australia", 3, 352, 360000.00);
    	Property propertyThree = new Property(3, "3 Boston Ave, Australia", 5, 800, 650000.00);
    	Property propertyFour = new Property(4, "3 Boston Ave, Australia", 2, 170, 435000.00);
    	Property propertyFive = new Property(5, "82 Boston Ave, Australia", 1, 60, 820000.00);
    	
    	try {
	    	
    		propertiesRepositroy.addProperty(propertyOne);
    		propertiesRepositroy.addProperty(propertyTwo);
    		propertiesRepositroy.addProperty(propertyThree);
    		propertiesRepositroy.addProperty(propertyFour);
	    	propertiesRepositroy.addProperty(propertyFive);
    	}catch (Exception e) {
    		System.err.println("Error message: " + e);
    	}
    		
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	try {
	    	for (Property property: propertiesRepositroy.getAllProperties()){
	    		System.out.println(property.toString());
	    	}
    	} catch(Exception e) {
    		System.err.println("Error message: " + e);
    	}
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	System.out.print("Enter the ID of the property you want to search: ");
    	
        Scanner scanner = new Scanner(System.in);
        
        try {
	        int idInput = scanner.nextInt();
	        Property property = propertiesRepositroy.searchPropertyById(idInput);
	        System.out.println(property.toString());
	    	
        } catch(Exception e) {
        	System.err.println("Error message: " + e);
        }
        finally {
        	scanner.close();
        }
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
